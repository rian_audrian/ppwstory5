function load_book_data(jQuery) {
	console.log("Starting log");
	$.ajax({
		url: '/get_book_data/',
		dataType: "json",
		success: function(response) {
			console.log("Grabbing info...");
			var entry = "<tbody>";
			response = response['items'];
			for (var i = 0; i < response.length; i++) {
				var title = response[i]["volumeInfo"]["title"];
				var publishInfo = response[i]["volumeInfo"]["publisher"];
				var authors = response[i]["volumeInfo"]["authors"].toString();
				var description = response[i]["volumeInfo"]["description"];
				var cover = response[i]["volumeInfo"]["imageLinks"]["smallThumbnail"];
				var star = 'https://www.iconfinder.com/icons/172558/download/png/32';
				console.log("Success " + i + "!");
				entry += "<tr>" +
					"<td>" + title + "</td>" +
					"<td>" + publishInfo + "</td>" +
					"<td>" + authors + "</td>" +
					"<td>" + description + "</td>" +
					"<td><img src='" + cover + "'></td>" +
					"</tr>";
				console.log("Appended " + i);
			}
			entry += "</tbody>";
			console.log("Appending entry...");
			$("#booktable").append(entry);
		}
	});
}

$(document).ready(load_book_data())