from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^story5', views.landing_page, name='landing_page'),
	url(r'^add-status', views.add_status, name='add-status'),
	url(r'^myprofile', views.profile_page, name='myprofile'),
	url(r'^books_page', views.books_page, name='books'),
	url(r'^get_book_data', views.get_book_data, name='get_book_data'),
	url(r'^subscribe', views.subscribe, name='subscribe'),
]