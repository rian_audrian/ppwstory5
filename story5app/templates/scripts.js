function start(jQuery) {

    var $j = jQuery.noConflict();

    $j("#nightmode-toggle").click(function(){
        $j("body").css('background', 'black');
        $j("body").css('color', 'white');
    });
    }

$(document).ready(start);