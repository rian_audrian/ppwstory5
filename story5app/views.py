from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from .forms import StatusForm, SubscriberForm
from .models import Status, Subscriber
import requests

# Create your views here.
response = {}
def landing_page(request):
	all_status = Status.objects.order_by("-created_date")
	response['form'] = StatusForm()
	response['status'] = all_status
	return render(request, 'landing_page.html', response)

def add_status(request):
	form = StatusForm(request.POST)

	if request.method=="POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			new_status = form.save()
			return HttpResponseRedirect('/story5/')

	else:
		form = StatusForm()

	response['form'] = form
	response['status'] = Status.objects.order_by("-created_date")
	return render(request, 'landing_page.html', response)

def profile_page(request):
	return render(request, 'myprofile.html')

def books_page(request):
	num_fav_books = request.session.get('num_fav_books', 0)

	context = {
		'num_fav_books': num_fav_books,
	}

	return render(request, 'books_page.html', context)

def get_book_data(request):
	if("q" in request.GET.keys()):
		key = request.GET['q']
		response = requests.get('https://www.googleapis.com/books/v1/volumes?q='+key)
		book_data = response.json()
		if (("totalItems" in book_data.keys()) and (int(book_data['totalItems']) > 0)):
			return JsonResponse(book_data)
		return JsonResponse({})

	response = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
	book_data = response.json()
	return JsonResponse(book_data)

def subscribe(request):
	form = SubscriberForm(request.POST)

	if request.method=="POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			new_user = form.save()
	else:
		form = SubscriberForm()

	response['form'] = form
	return render(request, 'subscribe.html', response)

def checkIsEmailAvailable(request):
	if Subscriber.objects.filter(email=email).count() > 0:
		return HttpResponse("EmailHasBeenUsed")
	return HttpResponse("EmailAvailable")