from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import Status, Subscriber, BookUser

# Register your models here.
admin.site.register(Status)
admin.site.register(Subscriber)

class BookUserInline(admin.StackedInline):
	model = BookUser
	can_delete = False
	verbose_name_plural = 'bookusers'

class UserAdmin(BaseUserAdmin):
	inlines = (BookUserInline,)

admin.site.unregister(User)
admin.site.register(User)
