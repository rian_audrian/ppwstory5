from django.db import models
from datetime import datetime
from django.contrib.auth.models import User

# Create your models here.
class BookUser(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	favorited_books_count = models.IntegerField(default=0)

class Status(models.Model):
	author = models.CharField(max_length = 100, default="Audrian Ananda")
	status = models.CharField(max_length = 300)
	created_date = models.DateTimeField(auto_now_add = True)

class Subscriber(models.Model):
	email = models.EmailField(max_length = 256)
	username = models.CharField(max_length = 30)
	password = models.CharField(max_length = 30)