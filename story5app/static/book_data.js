var staroff = 'https://img.icons8.com/ios/50/000000/christmas-star.png';
var staron = 'https://img.icons8.com/ios/50/f39c12/christmas-star-filled.png';
var count = 0;

function load_book_data(jQuery) {
	console.log("Starting log");
	$.ajax({
		url: '/get_book_data/',
		dataType: "json",
		success: function(response) {
			console.log("Grabbing info...");
			var entry = "<tbody id='innerbody'>";
			entry += cleanBookData(response);
			entry += "</tbody>";
			//console.log("Appending entry...");
			$("#booktable").append(entry);
			console.log("Entry appended");
		}
	});
}

function clickStar(id) {
	console.log("Star " + id + " clicked");
	var current = document.getElementById(id);
	$.ajax({
		url: '/update_fav_count/?q=' + current,
		success: function(response) {
			if(current.src == staroff) {
				console.log("Star on");
				count++;
				current.src = staron;
				console.log(count);
			} else {
				console.log("Star off");
				count--;
				current.src = staroff;
				console.log(count);
			}
			document.getElementById("counter").innerHTML = count;
		}
	})
}

function replaceQuilting() {
	var query = document.getElementById('replace').value;
	console.log(query);
	$.ajax({
		url: '/get_book_data/?q=' + query,
		dataType: 'json',
		success: function(response) {
			console.log(response);
			var entry = "";
			console.log("Grabbing info...");
			entry += cleanBookData(response);
			console.log("Appending entry...");
			document.getElementById("innerbody").innerHTML = entry;
			console.log("Entry appended");
		}
	})
}

function cleanBookData(input) {
	var output = "";
	rawData = input['items'];
	console.log(rawData.length)
	for (var i = 0; i < rawData.length; i++) {
		var volInfo = rawData[i].volumeInfo;
		var star = 'https://img.icons8.com/ios/50/000000/christmas-star.png';
		var coverImage = ("<img src=" + volInfo.imageLinks.thumbnail + ".png") || 'NO COVER';
		//console.log(volInfo.imageLinks.thumbnail + ".png");
		output +=
			"<td>" + volInfo.title + "</td>" +
			"<td>" + volInfo.publisher + "</td>" +
			"<td>" + volInfo.description + "</td>" +
			"<td>" + coverImage + "></td>" +
			"<td><img src=" + star + " id='star " + (i+1) + "' onClick='clickStar(id)'></td>" +
			"</tr>";
	}
	return output;
}

$(document).ready(load_book_data());
