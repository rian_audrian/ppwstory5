from django import forms	
from .models import Status, Subscriber

class StatusForm(forms.ModelForm):
	class Meta:
		model = Status
		fields = ['status']
		fields_required = ['status']
		widgets = {
			'status': forms.Textarea()
		}

class SubscriberForm(forms.ModelForm):
	class Meta:
		model = Subscriber
		fields = ['email', 'username', 'password']
		fields_required = ['email', 'username', 'password']
		widgets = {
			'email': forms.EmailInput(attrs={'class': 'form-control my-2', 'placeholder': 'Enter your email', 'id': 'username'}),
			'username': forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Enter your username', 'id': 'email'}),
			'password': forms.PasswordInput(attrs={'class': 'form-control my-2', 'placeholder': 'Enter your password', 'id': 'password'}),
		}
